#!/usr/bin/env python

__author__ = 'Carlos Rueda'


from mipm.client.process import Process


class SimpleProcess(Process):
    """
    Simple process for testing purposes.
    """

    def pm_shutdown(self):
        self._log("Ok, I'm being shutdown ;}")


if __name__ == '__main__':
    parser = Process.argument_parser
    opts = parser.parse_args()

    p = SimpleProcess(opts)
    p.start()
    p.join()
