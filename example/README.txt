README for the examples
=======================

example/config/simple_config.py
-------------------------------

This config file defines 2 simple processes, which should be running. They are
implemented with the example/proc/simple.py program.

From the main mict directory:

In a terminal, run the process manager:
$ bin/procman -c example/config/simple_config.yml

In another terminal, run a simple user program that makes some requests
to the process manager:
$ bin/python mipm/basic/components/user.py

The last request from the user program is 'shutdown' so all involved OS
processes (ie., the procman, the launched managed processes,
and the user program) should exit normally.


The output on the user program terminal is something like:

  USER      : Connecting to procman at tcp://localhost:55500 ...
  USER      : sending request to procman: get_state foo
  USER      : request sent
  USER      : Received reply from procman: OK:
Process foo running: True
  USER      : sending request to procman: get_state baz
  USER      : request sent
  USER      : Received reply from procman: OK:
Process baz running: True
  USER      : sending request to procman: get_state
  USER      : request sent
  USER      : Received reply from procman: OK:
Process foo running: True
Process baz running: True
  USER      : sending request to procman: shutdown
  USER      : request sent
  USER      : Received reply from procman: OK: shutdown
  USER      : END


while the output on the procman terminal, which combines output from the
procman itself (and its pproxys) and the managed processes:

2011-12-21 12:45:42.687 [scripts.procman: 32] INFO  : loading example/config/simple_config.yml
2011-12-21 12:45:42.695 [scripts.procman: 37] DEBUG : ManagerConfig(endpoint='tcp://*:55500', processes={'foo': ProcessDef(name='foo', command=['bin/python', 'example/proc/simple.py'], heartbeat_interval=2), 'baz': ProcessDef(name='baz', command=['bin/python', 'example/proc/simple.py'], heartbeat_interval=3)}, run=['foo', 'baz'], ports={'pxpr_base': 33300, 'pmpx_base': 44400})
2011-12-21 12:45:42.695 [scripts.procman: 39] INFO  : creating process manager...
  |PROCMAN: Binding tcp://*:55500 for user requests
  |PROCMAN: creating process proxys:
  |PROCMAN: _create_pproxy: ProcessDef(name='foo', command=['bin/python', 'example/proc/simple.py'], heartbeat_interval=2)
                                       |PPROXY/foo: accepting procman requests on port 44400 ...
                                       |PPROXY/foo: ready to publish events for procman on port 44401 ...
                                       |PPROXY/foo: Connecting to process at tcp://localhost:33300
                                       |PPROXY/foo: subscribing to events from process foo at tcp://localhost:33301
  |PROCMAN: Connecting to pproxy foo on port 44400
  |PROCMAN: listening to notifications from pproxy foo on port 44401 ...
  |PROCMAN: _create_pproxy: ProcessDef(name='baz', command=['bin/python', 'example/proc/simple.py'], heartbeat_interval=3)
                                       |PPROXY/baz: accepting procman requests on port 44402 ...
                                       |PPROXY/baz: ready to publish events for procman on port 44403 ...
                                       |PPROXY/baz: Connecting to process at tcp://localhost:33302
                                       |PPROXY/baz: subscribing to events from process baz at tcp://localhost:33303
  |PROCMAN: Connecting to pproxy baz on port 44402
  |PROCMAN: listening to notifications from pproxy baz on port 44403 ...
  |PROCMAN: starting myself
  |PROCMAN: starting pproxy: ProcessProxy(pdef=ProcessDef(name='foo', command=['bin/python', 'example/proc/simple.py'], heartbeat_interval=2))
  |PROCMAN: starting pproxy: ProcessProxy(pdef=ProcessDef(name='baz', command=['bin/python', 'example/proc/simple.py'], heartbeat_interval=3))
  |PROCMAN: dispatching ON processes:
  |PROCMAN: sent request to pproxy: run_process
                                       |PPROXY/foo: _run
                                       |PPROXY/foo: got procman request: run_process
                                       |PPROXY/foo: pm_run_process requested
                                       |PPROXY/foo: process launched, pid = 69759
                                       |PPROXY/baz: _run
                                                                              |PROCESS/foo: __init__
                                                                              |PROCESS/foo: binding REP socket for pproxy requests...
                                                                              |PROCESS/foo: _run
                                                                              |PROCESS/foo: process loop...
  |PROCMAN: Received reply from pproxy: OK: process launched, pid = 69759
  |PROCMAN: sent request to pproxy: run_process
                                       |PPROXY/baz: got procman request: run_process
                                       |PPROXY/baz: pm_run_process requested
                                       |PPROXY/baz: process launched, pid = 69760
                                                                              |PROCESS/baz: __init__
                                                                              |PROCESS/baz: binding REP socket for pproxy requests...
                                                                              |PROCESS/baz: _run
                                                                              |PROCESS/baz: process loop...
  |PROCMAN: Received reply from pproxy: OK: process launched, pid = 69760
  |PROCMAN: registering socket to poller, ProcessDef(name='foo', command=['bin/python', 'example/proc/simple.py', '--name', 'foo', '--heartbeat', '2', '--rep_port', '33300', '--pub_port', '33301'], heartbeat_interval=2)
  |PROCMAN: registering socket to poller, ProcessDef(name='baz', command=['bin/python', 'example/proc/simple.py', '--name', 'baz', '--heartbeat', '3', '--rep_port', '33302', '--pub_port', '33303'], heartbeat_interval=3)
                                                                              |PROCESS/foo: heartbeat sent: foo: 1
                                       |PPROXY/foo: event from process received: foo: 1
                                                                              |PROCESS/baz: heartbeat sent: baz: 1
                                       |PPROXY/baz: event from process received: baz: 1
                                                                              |PROCESS/foo: heartbeat sent: foo: 2
                                       |PPROXY/foo: event from process received: foo: 2
                                                                              |PROCESS/foo: heartbeat sent: foo: 3
                                       |PPROXY/foo: event from process received: foo: 3
                                                                              |PROCESS/baz: heartbeat sent: baz: 2
                                       |PPROXY/baz: event from process received: baz: 2
                                                                              |PROCESS/foo: heartbeat sent: foo: 4
                                       |PPROXY/foo: event from process received: foo: 4
                                                                              |PROCESS/baz: heartbeat sent: baz: 3
                                       |PPROXY/baz: event from process received: baz: 3
                                                                              |PROCESS/foo: heartbeat sent: foo: 5
                                       |PPROXY/foo: event from process received: foo: 5
                                                                              |PROCESS/foo: heartbeat sent: foo: 6
                                       |PPROXY/foo: event from process received: foo: 6
  |PROCMAN: got user request: get_state foo
  |PROCMAN: sent request to pproxy: get_state
                                       |PPROXY/foo: got procman request: get_state
                                       |PPROXY/foo: sent request to process: get_state
                                                                              |PROCESS/foo: request recv: get_state
                                                                              |PROCESS/baz: heartbeat sent: baz: 4
                                       |PPROXY/baz: event from process received: baz: 4
                                       |PPROXY/foo: Received reply from process: Process foo running: True
  |PROCMAN: Received reply from pproxy: Process foo running: True
                                                                              |PROCESS/foo: heartbeat sent: foo: 7
                                       |PPROXY/foo: event from process received: foo: 7
  |PROCMAN: got user request: get_state baz
  |PROCMAN: sent request to pproxy: get_state
                                       |PPROXY/baz: got procman request: get_state
                                       |PPROXY/baz: sent request to process: get_state
                                                                              |PROCESS/baz: request recv: get_state
                                       |PPROXY/baz: Received reply from process: Process baz running: True
  |PROCMAN: Received reply from pproxy: Process baz running: True
                                                                              |PROCESS/foo: heartbeat sent: foo: 8
                                       |PPROXY/foo: event from process received: foo: 8
  |PROCMAN: got user request: get_state
  |PROCMAN: sent request to pproxy: get_state
                                       |PPROXY/foo: got procman request: get_state
                                       |PPROXY/foo: sent request to process: get_state
                                                                              |PROCESS/foo: request recv: get_state
                                       |PPROXY/foo: Received reply from process: Process foo running: True
                                                                              |PROCESS/baz: heartbeat sent: baz: 5
                                       |PPROXY/baz: event from process received: baz: 5
  |PROCMAN: Received reply from pproxy: Process foo running: True
  |PROCMAN: sent request to pproxy: get_state
                                       |PPROXY/baz: got procman request: get_state
                                       |PPROXY/baz: sent request to process: get_state
                                                                              |PROCESS/baz: request recv: get_state
                                       |PPROXY/baz: Received reply from process: Process baz running: True
                                                                              |PROCESS/foo: heartbeat sent: foo: 9
                                       |PPROXY/foo: event from process received: foo: 9
  |PROCMAN: Received reply from pproxy: Process baz running: True
  |PROCMAN: got user request: shutdown
  |PROCMAN: sent request to pproxy: shutdown
                                       |PPROXY/foo: got procman request: shutdown
                                       |PPROXY/foo: pm_shutdown requested
                                       |PPROXY/foo: sent request to process: shutdown
                                                                              |PROCESS/foo: request recv: shutdown
                                                                              |PROCESS/foo: _keep_running set to False
                                                                              |PROCESS/foo: Ok, I'm being shutdown ;}
                                       |PPROXY/foo: Received reply from process: OK: shutdown
                                                                              |PROCESS/baz: heartbeat sent: baz: 6
                                       |PPROXY/baz: event from process received: baz: 6
  |PROCMAN: Received reply from pproxy: OK: shutdown
  |PROCMAN: sent request to pproxy: shutdown
                                       |PPROXY/baz: got procman request: shutdown
                                       |PPROXY/baz: pm_shutdown requested
                                       |PPROXY/baz: sent request to process: shutdown
                                                                              |PROCESS/baz: request recv: shutdown
                                                                              |PROCESS/baz: _keep_running set to False
                                                                              |PROCESS/baz: Ok, I'm being shutdown ;}
                                       |PPROXY/baz: Received reply from process: OK: shutdown
  |PROCMAN: Received reply from pproxy: OK: shutdown
