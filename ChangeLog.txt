mict – MI Container
Carlos Rueda - MBARI

2012-01-12
    - Recently added coverage and Doxygen stuff.

2011-12-21
    - enabled gevent in actual components.

    - more complete process management in terms of supporting functionality.
      The procman can now launch the processes defined in the configuration
      file and is able to accept requests from an external client program
      (so-called 'user' here) and dispatch simple commands to the managed
      processes (including shutting them down) via corresponding process
      proxies.

      See example/README.txt

2011-12-20
    - Note about gevent_zeromq and the Poller.poll() method:
      poll() is used in server.procman and server.pproxy.
      The specific call is poller.poll(timeout=200),
      where the specific timeout value is small (a fraction of a sec) but
      arbitrary. I'm using an explicit timeout because without the timeout
      the poll will block the whole thread; but I didn't expect this behavior
      according to the general gevent_zeromq documentation. That is,
      the poll() call (without timeout) should only block the calling
      greenlet, thus allowing the other greenlets to run. It turns out the
      Poller class has not yet been prepared to be "green". See this pull
      request: https://github.com/traviscline/gevent-zeromq/pull/20.
      So, for the time being, the timeout and the associated sleep() (as a
      cooperative yield) serve as a workaround, but note that it makes the
      process take more CPU than necessary.
     
2011-12-19
    - basic messaging setting with 4 main components: process, pproxy, procman,
      and user.
    - basic process: use poller with timeout (instead of recv(zmq.NOBLOCK)) to
      make much more efficient use of the CPU. 

2011-12-12
    - basic messaging setting with 4 main components: process, pproxy, procman,
      and user.
    
2011-12-08
    - some renamings reflecting similar aspects in Antelope's rtexec.

2011-12-07
    - preliminary elements including pyzmq.

2011-12-01
	- initial buildout preparations.