#!/usr/bin/env python

# adapted from pyon's

try:
    from setuptools import setup, find_packages
except ImportError:
    from distutils.core import setup

import os
import sys

# Add /usr/local/include to the path for macs, fixes easy_install for several packages (like gevent and pyyaml)
if sys.platform == 'darwin':
    os.environ['C_INCLUDE_PATH'] = '/usr/local/include'

version = '0.1.0'

setup(  name = 'mict',
        version = version,
        description = 'Marine Integration Container',
        url = 'https://bitbucket.org/carueda/mict',
        download_url = 'http://ooici.net/releases',
        license = 'Apache 2.0',
        author = 'Carlos Rueda',
        author_email = 'carueda@mbari.org',
        keywords = ['ooici', 'mict'],
        packages = find_packages(),
        entry_points = {
            'console_scripts' : [
                'pycc=scripts.pycc:entry',
                'control_cc=scripts.control_cc:main',
                'generate_interfaces=scripts.generate_interfaces:main'
                ]
            },
        dependency_links = [
            'http://ooici.net/releases'
        ],
        test_suite = 'mict',
        install_requires = [
            'argparse==1.2.1',
            # 'greenlet==0.3.1',
            'gevent==0.13.6',
            'simplejson==2.1.6',
            'msgpack-python==0.1.9',
            #'setproctitle==1.1.2',
            'pyyaml==3.10',
            'pika==0.9.5',
            #'httplib2==0.7.1',
            'pyzmq==2.1.10',
            'gevent_zeromq==0.2.0',
            #'HTTP4Store==0.3.1',
            'zope.interface',
            #'couchdb==0.8',
            # 'lockfile==0.9.1',
            'python-daemon==1.6',
            #'bottle==0.9.6',
            #'M2Crypto==0.21.1-pl1',
            'coverage==3.5',
            'nose==1.1.2',
            #'ipython==0.11',
            'readline==6.2.1',
            'mock',
        ],
     )
