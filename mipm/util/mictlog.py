#!/usr/bin/env python

__author__ = 'Carlos Rueda'


import logging
import logging.config

LOG_FILE = 'log.conf'

logging.config.fileConfig(LOG_FILE)


def getLogger(name):
#    logging.basicConfig(format='%(levelname)s - %(name)s  - %(message)s',
#                        level=logging.DEBUG)
    log = logging.getLogger(name)
    return log
