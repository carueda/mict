#!/usr/bin/env python

__author__ = 'Carlos Rueda'

"""
Common process-related elements for the server (ie., process manager) and
the client.
"""

from zope.interface import Interface, implements


class IProcess(Interface):
    """
    Interface for the processes that can be managed.
    Actual processes should extend base class Process in the client package.
    """

    #TODO this is all very preliminary

    def pm_get_state():
        """
        Returns a the state of this process; this is a state of its execution
        from the process management point of view.
        """
        pass

    def pm_shutdown():
        """
        Called to notify this process that it is being shutdown.
        """
        pass


class ProcessDef(object):
    """
    Process definition.
    """

    def __init__(self, **kwargs):
        assert 'name' in kwargs
        self._name = kwargs.get('name')
        self._cmd = kwargs.get('cmd', self._name + "_cmd")
        self._heartbeat_interval = kwargs.get('heartbeat', 10)

    @property
    def name(self):
        """Name that identifies the process"""
        return self._name

    @property
    def cmd(self):
        """Command to execute the process"""
        return self._cmd

    @property
    def heartbeat_interval(self):
        """heartbeat interval in seconds"""
        return self._heartbeat_interval

    def __repr__(self):
        return "%s(name=%r, command=%r, heartbeat_interval=%r)" % (
        self.__class__.__name__, self.name, self.cmd,
        self.heartbeat_interval)
