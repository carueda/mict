mimp/basic/components README

This package exercises basic communication mechanisms for the main components
of the MI execution:


   +------------------+                       +----------+
   |                  |          5550         |          |
   |      procman     |REP <-------------+ REQ|   user   |
   |                  |                       |          |
   +------------------+                       +----------+
     REQ           SUB
      +             ^
      |             |
      |4440         |4445
      |             |
      |             |
      v             +
     REP           PUB
    +-----------------+         3335          +------------+
    |                 |SUB <-------------+ PUB|            |
    |                 |                       |            |
    |     pproxy      |                       |   process  |
    |                 |         3330          |            |
    |                 |REQ <-------------+ REP|            |
    +-----------------+                       +------------+


The interaction per se is very simple and uses hard-coded parameters (interface
endpoints, name of the process). The main goal is to exercise the zeromq
messaging mechanism that can be used to enable the communication. The starting
of the process from the process manager (via the process proxy) is not included
in this basic test.

process:
    loop with a poll over a rep_socket. A timeout is specified to allow
    the generation of heartbeats to the PUB socket

pproxy:
    loop with a poll over the SUB and REP sockets

procman:
    loop with a poll over the SUB and REP sockets

user:
    REQ socket to send requests to the procman

NOTE:
  - the pproxy and procman programs are launched as separate OS processes in
    this test but they can actually be part of a single OS process.
  - all endpoints use TCP in this simple setting (but IPC or INPROC can be
    used as appropriate).

From the root directory, the interaction can be run on a single terminal
session as follows:
  $ scripts/basiccomps.sh
This script is simply:
  bin/python mipm/basic/components/process.py &
  bin/python mipm/basic/components/pproxy.py &
  bin/python mipm/basic/components/procman.py &
  bin/python mipm/basic/components/user.py
(but the launch order is not strict per zeromq capabilities).

The output looks something like:

  PPROXY    : listening to hearbeats and events from process...
  PPROXY    : accepting procman requests...
  PPROXY    : Connecting to process...
  PROCMAN   : listening to async notifications from pproxy...
  PROCMAN   : accepting user requests...
  PROCMAN   : Connecting to pproxy...
  USER      : Connecting to procman...
  PROCESS   : binding REP socket for pproxy requests...
  PROCESS   : process loop...
  USER      : sent request to procman: dummyrequest1
  PROCMAN   : got user request: dummyrequest1
  USER      : Received reply from procman: response to user's request dummyrequest1
  USER      : sent request to procman: dummyrequest2
  PROCMAN   : got user request: dummyrequest2
  USER      : Received reply from procman: response to user's request dummyrequest2
  USER      : sent request to procman: shutdown
  PROCMAN   : got user request: shutdown
  PROCMAN   : sent request to pproxy: shutdown
  USER      : Received reply from procman: response to user's request shutdown
  USER      : END
  PPROXY    : got procman request: shutdown
  PPROXY    : sent request to process: shutdown
  PROCMAN   : Received reply from pproxy: response to procman's request shutdown
  PROCMAN   : END
  PROCESS   : request recv: shutdown
  PROCESS   : END
  PPROXY    : Received reply from process: process's response to pproxy's request shutdown
  PPROXY    : END
