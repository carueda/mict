#!/usr/bin/env python

"""
Basic messaging mechanisms for the procman.
This consists of setting:
- SUB socket to receive async notifications from pproxy instances
- REP socket to dispatch requests from users
- For each managed process:
  - REQ socket to send requests to corresp. pproxy

Note: for this basic mechanism only one managed process is considered.
"""

__author__ = 'Carlos Rueda'

import zmq

PROCESS_NAME = "foo"
FILTER = PROCESS_NAME + ":"
SUB_PORT = 4445
REP_PORT = 55500
REQ_PORT = 4440


def _log(m):
    print '  %-10s: %s' % ('PROCMAN', m)

#  Socket to talk to server
context = zmq.Context()

#
# sockets
#

# subscribe to async notifications from pproxy
sub_socket = context.socket(zmq.SUB)
_log("listening to async notifications from pproxy...")
sub_socket.connect("tcp://localhost:%d" % SUB_PORT)
sub_socket.setsockopt(zmq.SUBSCRIBE, FILTER)

# REP socket to dispatch user requests
rep_socket = context.socket(zmq.REP)
_log("accepting user requests...")
rep_socket.bind("tcp://*:%d" % REP_PORT)

# socket to make requests to pproxy
_log("Connecting to pproxy...")
req_socket = context.socket(zmq.REQ)
req_socket.connect("tcp://localhost:%d" % REQ_PORT)


def rpc_to_pproxy(cmd):
    request = str(cmd)
    req_socket.send(request)
    _log("sent request to pproxy: %s" % request)

    # actual implementation would avoid blocking in some way
    message = req_socket.recv()
    _log("Received reply from pproxy: %s" % message)
    return message


#
# poller
#
poller = zmq.Poller()
poller.register(sub_socket, zmq.POLLIN)
poller.register(rep_socket, zmq.POLLIN)

# loop
while True:
    socks = dict(poller.poll())

    if rep_socket in socks and socks[rep_socket] == zmq.POLLIN:
        # got request from user.
        user_request = rep_socket.recv()
        _log('got user request: %s' % user_request)
        # just reply with an ad hoc message
        rep_socket.send("response to user's request %s" % user_request)
        if user_request == "shutdown":
            rpc_to_pproxy('shutdown')
            break

    if sub_socket in socks and socks[sub_socket] == zmq.POLLIN:
        # got event from pproxy.
        pproxy_event = sub_socket.recv()
        # actual implementation would do something with the event
        _log('pproxy event recv: %s' % pproxy_event)

_log('END')
