#!/usr/bin/env python

"""
Basic messaging mechanisms for a pproxy.
This consists of setting:
- SUB socket to receive heartbeats and events from process
- REP socket to dispatch requests from procman
- REQ socket to send requests to process
- PUB socket to publish events to procman
"""

__author__ = 'Carlos Rueda'

import zmq

PROCESS_NAME = "foo"
FILTER = PROCESS_NAME + ":"
SUB_PORT = 3335
REP_PORT = 4440
REQ_PORT = 3330
PUB_PORT = 4445


def _log(m):
    print '  %-10s: %s' % ('PPROXY', m)

context = zmq.Context()

#
# sockets
#

# subscribe to heartbeats and events from the process
sub_socket = context.socket(zmq.SUB)
_log("listening to hearbeats and events from process...")
sub_socket.connect("tcp://localhost:%d" % SUB_PORT)
sub_socket.setsockopt(zmq.SUBSCRIBE, FILTER)

# REP socket to dispatch procman requests
rep_socket = context.socket(zmq.REP)
_log("accepting procman requests...")
rep_socket.bind("tcp://*:%d" % REP_PORT)

# socket to make requests to process
_log("Connecting to process...")
req_socket = context.socket(zmq.REQ)
req_socket.connect("tcp://localhost:%d" % REQ_PORT)


def rpc_to_process(cmd):
    request = str(cmd)
    req_socket.send(request)
    _log("sent request to process: %s" % request)

    # actual implementation would avoid blocking in some way
    message = req_socket.recv()
    _log("Received reply from process: %s" % message)
    return message

# socket to publish events to procman
#actual impl would publish actual events, for example to notify that the
# process died and could not be started.
pub_socket = context.socket(zmq.PUB)
pub_socket.bind("tcp://*:%d" % PUB_PORT)


#
# poller
#
poller = zmq.Poller()
poller.register(sub_socket, zmq.POLLIN)
poller.register(rep_socket, zmq.POLLIN)

# loop
while True:
    socks = dict(poller.poll())

    if rep_socket in socks and socks[rep_socket] == zmq.POLLIN:
        # got request from procman.
        procman_request = rep_socket.recv()
        _log('got procman request: %s' % procman_request)
        # just reply with an ad hoc message
        rep_socket.send("response to procman's request %s" % procman_request)
        if procman_request == "shutdown":
            rpc_to_process('shutdown')
            break

    if sub_socket in socks and socks[sub_socket] == zmq.POLLIN:
        # got heartbeat or event from process.
        process_event = sub_socket.recv()
        # actual implementation would do something with the event
        _log('heartbeat or event from process recv: %s' % process_event)

_log('END')

