#!/usr/bin/env python

"""
Simple 'user' program to exercise basic messaging mechanisms with a process
manager. This program consists of setting a REQ socket to send requests to
the procman. No actual interaction with the user is actually enabled (except
for optional passed arguments on the command line); this programs just sends a
sequence of requests and then a final "shutdown" command.

See example/config/simple_config.yml
"""

__author__ = 'Carlos Rueda'

import argparse
import zmq
import time


DEFAULT_PROCMAN_ADDDRESS = "tcp://localhost:55500"
DEFAULT_REQUESTS = ["get_state foo", "get_state baz", "get_state", "shutdown"]


def entry():
    parser = argparse.ArgumentParser(description="simple user program")
    parser.add_argument("-pm", "--procman",
                        help="procman address (default: %s)" %
                             DEFAULT_PROCMAN_ADDDRESS,
                        default=DEFAULT_PROCMAN_ADDDRESS)

    parser.add_argument("-r", "--requests",
                        nargs="+",
                        help="list of requests (default: %s)" %
                             DEFAULT_REQUESTS,
                        default=DEFAULT_REQUESTS)
    opts = parser.parse_args()

    procman_address = opts.procman
    requests = opts.requests

    if "shutdown" not in requests:
        requests.append("shutdown")

    context = zmq.Context()

    def _log(m):
        print '  %-10s: %s' % ('USER', m)

    # socket to make requests to procman
    _log("Connecting to procman at %s ..." % procman_address)
    req_socket = context.socket(zmq.REQ)
    req_socket.connect(procman_address)

    def rpc(cmd):
        request = str(cmd)
        _log("sending request to procman: %s" % request)
        req_socket.send(request)
        _log("request sent")

        # TODO set and handle timeout
        message = req_socket.recv()
        _log("Received reply from procman: %s" % message)

    for request in requests:
        time.sleep(1)
        rpc(request)
        if request == "shutdown":
            break

    _log('END')


if __name__ == '__main__':
    entry()
