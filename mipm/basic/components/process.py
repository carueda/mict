#!/usr/bin/env python

"""
Basic messaging mechanisms for a process.
This consists of:
- setting a REP socket to accept requests from corresponding pproxy.
  A zmq.Poller is used over this socket to do a poll with a timeout
  corresponding to the heartbeat interval.
- publishing a heartbeat
"""

__author__ = 'Carlos Rueda'

import zmq
import time


PROCESS_NAME = "foo"
HEARTBEAT_INTERVAL_SECS = 2
PUB_PORT = 3335
REP_PORT = 3330


def _log(m):
    print '  %-10s: %s' % ('PROCESS', m)

context = zmq.Context()

#
# sockets
#

# socket to publish heartbeats or other events
pub_socket = context.socket(zmq.PUB)
pub_socket.bind("tcp://*:%d" % PUB_PORT)

heartbeat_count = 0

def heartbeat():
    global heartbeat_count
    heartbeat_count += 1
    msg = "%s: %d" % (PROCESS_NAME, heartbeat_count)
    pub_socket.send(msg)
    _log('heartbeat sent: %s' % msg)


# bind REP socket to reply to pproxy requests
rep_socket = context.socket(zmq.REP)
_log("binding REP socket for pproxy requests...")
rep_socket.bind("tcp://*:%d" % REP_PORT)

# use poller over the rep_socket, so we can try a reception with a timeout, and
# thus avoid using recv(zmq.NOBLOCK), which would take a lot of CPU.
poller = zmq.Poller()
poller.register(rep_socket, zmq.POLLIN)

_log("process loop...")
heartbeat_at = time.time() + HEARTBEAT_INTERVAL_SECS
while True:
    request = None

    socks = dict(poller.poll(timeout=(1000 * HEARTBEAT_INTERVAL_SECS)))
#    _log("after poll,  socks = %s" % socks)

    if rep_socket in socks and socks[rep_socket] == zmq.POLLIN:
        request = rep_socket.recv()

    if time.time() > heartbeat_at:
        heartbeat()
        heartbeat_at = time.time() + HEARTBEAT_INTERVAL_SECS

    if request is not None:
        _log('request recv: %s' % request)
        # actual implementation will dispatch pproxy's request
        rep_socket.send("process's response to pproxy's request %s" % request)
        if request == "shutdown":
            break

_log('END')
