import unittest   
import yaml
 
class Test(unittest.TestCase):

    def setUp(self):
        self.doc = "a: 1\nb:\n  c: 3\n  d: 4\n"

    def test(self):
        self.assertEquals(self.doc, yaml.dump(yaml.load(self.doc), default_flow_style=False))


if __name__ == '__main__':
    unittest.main()
