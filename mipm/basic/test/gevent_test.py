#!/usr/bin/env python

import gevent
from gevent import Greenlet


class SimpleGreenlet(Greenlet):

    def __init__(self, interval):
        Greenlet.__init__(self)
        self.interval = interval
        self.times = 0

    def _run(self):
        while True:
            gevent.sleep(self.interval)
            self.times += 1
            print "RUN: %s" % self

    def __str__(self):
        return 'SimpleGreenlet(interval=%d, times=%d)' % (self.interval,
                                                          self.times)


if __name__ == '__main__':
    g = SimpleGreenlet(2)
    g.start()
    gevent.sleep(8)
    g.kill()
    print 'g.dead = %s' % g.dead
