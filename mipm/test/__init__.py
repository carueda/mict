#!/usr/bin/env python

__author__ = 'Carlos Rueda'
__license__ = 'Apache 2.0'


from unittest import TestCase
from mipm.server.config import ManagerConfig
#from unittest import SkipTest

import yaml


man_config_yaml = """
endpoint: ipc://procman

processes:
- name: foo0
  cmd: [foocmd0]
  heartbeat: 0
- name: foo1
  cmd: [foocmd1]
  heartbeat: 1

run:
- foo0: ON
- foo1: OFF

ports:
  # base number for ports between procman and pproxys
  pmpx_base: 44400
  # base number for ports between pproxys and processes
  pxpr_base: 33300

"""

class BaseMictTestCase(TestCase):

    def setUp(self):
        pass

    def tearDown(self):
        pass



    def _get_manager_config(self):

        pyobj = yaml.load(man_config_yaml)
        config = ManagerConfig(pyobj)
        return config

