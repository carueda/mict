#!/usr/bin/env python

__author__ = 'Carlos Rueda'

"""
Defines the main ProcessManager class.
"""

from mipm.server.config import ManagerConfig
from mipm.server.pproxy import ProcessProxy

from gevent_zeromq import zmq
from gevent import Greenlet
from gevent import sleep

from mipm.util.mictlog import getLogger
log = getLogger(__name__)


def _log(m):
    """ad hoc provisional logger"""
    name = '|PROCMAN'
    print '%10s: %s' % (name, m)


class ProcessManager(Greenlet):
    """
    The main process manager component. It delegates in ProcessProxy instances
    to manage corresponding processes. All interaction with the ProcessProxy
    instances is via messaging.
    """

    def __init__(self, config):
        """
        - Loads the configuration and prepares the zmq context.
        - creates the pproxys
        """
        Greenlet.__init__(self)

        self._keep_running = True

        self.config = config
        self._validate_configuration()

        self.context = zmq.Context()

        # REP socket to dispatch user requests
        self.rep_socket = self.context.socket(zmq.REP)
        _log("Binding %s for user requests" % self.config.endpoint)
        self.rep_socket.bind(self.config.endpoint)

        self._create_pproxys()

        _log("__init__ complete")

    def _validate_configuration(self):
        assert isinstance(self.config, ManagerConfig)

    def _create_pproxys(self):
        _log("creating process proxys:")

        self.pproxys = {}

        pmpx_base = int(self.config.ports['pmpx_base'])
        pxpr_base = int(self.config.ports['pxpr_base'])

        for proc_def in self.config.processes.values():
            # the 4 needed ports
            pmpx_req_port = pmpx_base + 0
            pmpx_sub_port = pmpx_base + 1
            pxpr_req_port = pxpr_base + 0
            pxpr_sub_port = pxpr_base + 1

            ppinfo = self._create_pproxy(proc_def,
                                         pmpx_req_port, pmpx_sub_port,
                                         pxpr_req_port, pxpr_sub_port)
            self.pproxys[proc_def.name] = ppinfo

            pmpx_base += 2
            pxpr_base += 2

    def _create_pproxy(self, proc_def,
                       pmpx_req_port, pmpx_sub_port,
                       pxpr_req_port, pxpr_sub_port):
        """
        Creates and returns a dictionary with:
          "pproxy"     => the ProcessProxy instance
          "sub_socket" => socket to listen to async events from pproxy
          "req_socket" => socket to make requests to pproxy

        @param proc_def process definition
        @param pmpx_req_port port for the req_socket
        @param pmpx_sub_port port for the sub_socket
        """
        _log("_create_pproxy: %r" % proc_def)

        # the dict to be returned
        ppinfo = {}

        #
        # note that the pmpx_req_port here will correspond to the pmpx_rep_port
        # in the ProcessProxy; similarly for pmpx_sub_port (corresp. to
        # pmpx_pub_port):
        #
        pproxy = ProcessProxy(self.context,
                              pmpx_req_port, pmpx_sub_port,
                              pxpr_req_port, pxpr_sub_port,
                              proc_def)

        proc_name = proc_def.name

        # socket to make requests to pproxy
        _log("Connecting to pproxy %s on port %s" % (proc_name, pmpx_req_port))
        req_socket = self.context.socket(zmq.REQ)
        req_socket.connect("tcp://localhost:%d" % pmpx_req_port)
        ppinfo['req_socket'] = req_socket

        # socket to subscribe to async notifications from pproxy
        sub_socket = self.context.socket(zmq.SUB)
        _log("listening to notifications from pproxy %s on port %s ..." %
             (proc_name, pmpx_sub_port))
        sub_socket.connect("tcp://localhost:%d" % pmpx_sub_port)
        sub_socket.setsockopt(zmq.SUBSCRIBE, "%s:" % proc_name)
        ppinfo['sub_socket'] = sub_socket

        ppinfo['pproxy'] = pproxy

        return ppinfo

    def start(self):
        _log("starting myself")
        super(ProcessManager, self).start()
        for proc_name in self.config.run_processes:
            ppinfo = self.pproxys.get(proc_name)
            if ppinfo:
                pproxy = ppinfo['pproxy']
                _log("starting pproxy: %s" % pproxy)
                pproxy.start()

        return len(self.pproxys)

    def _dispatch_processes(self):
        self._dispatch_on_processes()
        self._dispatch_off_processes()

    def _dispatch_on_processes(self):
        """
        Dispatches the processes that should be running,
        starting them as necessary.
        """
        _log("dispatching ON processes:")
        for proc_name in self.config.run_processes:
            ppinfo = self.pproxys.get(proc_name)
            if ppinfo:
                req_socket = ppinfo['req_socket']
                self.__rpc_to_pproxy(req_socket, 'run_process')

    def _dispatch_off_processes(self):
        """
        Dispatches the processes that should not be running,
        stopping them as necessary.
        """
        off_procs = set(self.config.processes.keys()) -\
                    set(self.config.run_processes)
        if len(off_procs) == 0:
            return

        _log("dispatching OFF processes:")
        for proc_name in off_procs:
            ppinfo = self.pproxys.get(proc_name)
            if ppinfo:
                req_socket = ppinfo['req_socket']
                #
                # TODO dispatch OFF process appropriately.
                # For the moment, doing nothing.
                ## self.__rpc_to_pproxy(req_socket, 'shutdown')
                _log("TODO: dispatch OFF process %s" % ppinfo['pproxy'].pdef)

    def __rpc_to_pproxy(self, req_socket, cmd):
        request = str(cmd)
        req_socket.send(request)
        _log("sent request to pproxy: %s" % request)

        # actual implementation would avoid blocking in some way
        message = req_socket.recv()
        _log("Received reply from pproxy: %s" % message)
        return message

    def __str__(self):
        return '%s(pdefs=%s)' % (self.__class__.__name__, "pdef-TODO")

    def _run(self):
        """
        Main dispatching routing.
        - dispatches the pproxys according to the configuration:
           - running the processes that should be ON
           - shutting down the processes that should be OFF
        - goes to the main dispatch loop
        """

        _log("_run ****")

        self._dispatch_processes()

        #
        # poller
        #
        poller = zmq.Poller()
        poller.register(self.rep_socket, zmq.POLLIN)

        for proc_name in self.config.run_processes:
            ppinfo = self.pproxys.get(proc_name)
            if ppinfo:
                pdef = ppinfo['pproxy'].pdef
                sub_socket = ppinfo.get('sub_socket')
                _log("registering socket to poller, %s" % pdef)
                poller.register(sub_socket, zmq.POLLIN)

        while self._keep_running:
            #
            # about the timeout and the sleep() call below,
            # see note in ChangeLog.txt
            #
            socks = dict(poller.poll(timeout=200))

            if self.rep_socket in socks and socks[self.rep_socket] == \
                                            zmq.POLLIN:
                # got request from user.
                user_request = self.rep_socket.recv()
                response = self._handle_user_request(user_request)
                self.rep_socket.send(response)

            for ppinfo in self.pproxys.values():
                sub_socket = ppinfo.get('sub_socket')
                if sub_socket in socks and socks[sub_socket] == \
                                                zmq.POLLIN:
                    # got event from one of the pproxys.
                    pproxy_event = self.sub_socket.recv()
                    pproxy = ppinfo['pproxy']
                    self._handle_pproxy_event(pproxy, pproxy_event)

            # can be removed when the Poller class becomes green.
            sleep()  # "the canonical way of expressing a cooperative yield"

    def _handle_pproxy_event(self, pproxy, pproxy_event):
        _log("pproxy %s sent event %s" % (pproxy, pproxy_event))

    def _handle_user_request(self, user_request):
        _log('got user request: %s' % user_request)

        if user_request.startswith("get_state"):
            toks = user_request.split()
            proc_name = toks[1] if len(toks) > 1 else None
            return self._get_state(proc_name)

        if user_request == "shutdown":
            return self._shutdown()

        # todo other requests.

        return "OK: to %s " % user_request

    def _get_state(self, proc_name=None):
        ppinfos = []
        if proc_name is not None:
            if proc_name in self.config.run_processes:
                ppinfo = self.pproxys.get(proc_name)
                assert ppinfo is not None
                ppinfos.append(ppinfo)
        else:
            for proc_name in self.config.run_processes:
                ppinfo = self.pproxys.get(proc_name)
                if ppinfo:
                    ppinfos.append(ppinfo)

        string = ""
        for ppinfo in ppinfos:
            pproxy = ppinfo['pproxy']
            if pproxy.pdef.name in self.config.run_processes:
                req_socket = ppinfo['req_socket']
                ppstate = self.__rpc_to_pproxy(req_socket, 'get_state')
                string = "%s\n%s" % (string, ppstate)

        return "OK: %s" % string

    def _shutdown(self):
        for proc_name in self.config.run_processes:
            ppinfo = self.pproxys.get(proc_name)
            if ppinfo:
                req_socket = ppinfo['req_socket']
                self.__rpc_to_pproxy(req_socket, 'shutdown')

        # TODO other clean-up

        self._keep_running = False

        return "OK: shutdown"
