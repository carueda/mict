#!/usr/bin/env python

__author__ = 'Carlos Rueda'
__license__ = 'Apache 2.0'


import unittest

from mipm.test import BaseMictTestCase


class TestConfig(BaseMictTestCase):


    def test_manager_config(self):

        config = self._get_manager_config()

        self.assertEquals(config.endpoint, "ipc://procman")

        procdefs = config.processes.values()
        self.assertEquals(len(procdefs), 2)

        for i in range(len(procdefs)):
            self.assertEquals(procdefs[i].name, "foo%d" % i)
            self.assertEquals(procdefs[i].cmd, ["foocmd%d" % i])
            self.assertEquals(procdefs[i].heartbeat_interval, i)





if __name__ == '__main__':
    unittest.main()
