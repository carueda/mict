#!/usr/bin/env python

__author__ = 'Carlos Rueda'
__license__ = 'Apache 2.0'


import unittest

from mipm.test import BaseMictTestCase
from mipm.server.procman import ProcessManager




class TestProcMan(BaseMictTestCase):


    def test_start(self):

        config = self._get_manager_config()
        proc_man = ProcessManager(config)

        proc_man.start()


if __name__ == '__main__':
    unittest.main()
