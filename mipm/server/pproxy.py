#!/usr/bin/env python

__author__ = 'Carlos Rueda'

"""
Process management on the server side.
"""

from mipm.process import ProcessDef

from gevent_zeromq import zmq
from gevent import Greenlet
from gevent import sleep

import subprocess

from mipm.util.mictlog import getLogger
log = getLogger(__name__)


class ProcessProxy(Greenlet):
    """
    An instance of this class handles the dispatch and interaction with a
    running process on the client side.
    """

    def _log(self, m):
        """ad hoc provisional logger"""
        name = '|%s/%s' % ('PPROXY', self.pdef.name)
        print '%50s: %s' % (name, m)

    def __init__(self, context,
                 pmpx_rep_port, pmpx_pub_port,
                 pxpr_req_port, pxpr_sub_port,
                 pdef):
        """
        Creates a pproxy instance to control a managed process. A manages
        process is launched as a separate OS process. All interaction
        with the process is via messaging.

        @param context zmq context
        @param pmpx_rep_port socket port used to dispatch procman requests
        @param pmpx_pub_port socket port used to publish events to procman
        @param pxpr_req_port socket port used to make requests to process
        @param pxpr_sub_port socket port used subscribe to events from process
        @pdef process definition
        """

        Greenlet.__init__(self)

        assert isinstance(pdef, ProcessDef)
        self.pdef = pdef

        self._keep_running = True

        #
        # sockets related with procman
        #

        # REP socket to dispatch procman requests
        self.rep_socket = context.socket(zmq.REP)
        self._log("accepting procman requests on port %s ..." % pmpx_rep_port)
        self.rep_socket.bind("tcp://*:%d" % pmpx_rep_port)

        # socket to publish events to procman
        #TODO publish actual events, for example an event to notify that the
        # process died and could not be started.
        self.pub_socket = context.socket(zmq.PUB)
        self._log("ready to publish events for procman on port %s ..."
                  % pmpx_pub_port)
        self.pub_socket.bind("tcp://*:%d" % pmpx_pub_port)

        #
        # sockets related with managed process
        #

        # store these as we need them as params for launching the process
        self.pxpr_req_port = pxpr_req_port
        self.pxpr_sub_port = pxpr_sub_port

        # socket to make requests to process
        req_address = "tcp://localhost:%d" % pxpr_req_port
        self._log("Connecting to process at %s" % req_address)
        self.req_socket = context.socket(zmq.REQ)
        self.req_socket.connect(req_address)

        # subscribe to heartbeats and events from the process
        sub_address = "tcp://localhost:%d" % pxpr_sub_port
        self.sub_socket = context.socket(zmq.SUB)
        self._log("subscribing to events from process %s at %s" %
             (self.pdef.name, sub_address))
        self.sub_socket.connect(sub_address)
        self.sub_socket.setsockopt(zmq.SUBSCRIBE, self.pdef.name + ":")

        # the external OS process
        self.os_proc = None

    def __str__(self):
        return '%s(pdef=%s)' % (self.__class__.__name__, str(self.pdef))

    def _run(self):
        """
        Main dispatching loop.
        """

        self._log("_run")

        #
        # poller
        #
        poller = zmq.Poller()
        poller.register(self.rep_socket, zmq.POLLIN)
        poller.register(self.sub_socket, zmq.POLLIN)

        while self._keep_running:
            #
            # about the timeout and the sleep() call below,
            # see note in ChangeLog.txt
            #
            socks = dict(poller.poll(timeout=200))

            if self.rep_socket in socks and socks[self.rep_socket] == \
                                            zmq.POLLIN:
                # got request from procman.
                procman_request = self.rep_socket.recv()
                response = self._handle_procman_request(procman_request)
                self.rep_socket.send(response)

            if self.sub_socket in socks and socks[self.sub_socket] == \
                                            zmq.POLLIN:
                # got heartbeat or event from process.
                process_event = self.sub_socket.recv()
                self._handle_process_event(process_event)

            # can be removed when the Poller class becomes green.
            sleep()  # "the canonical way of expressing a cooperative yield"

    def _handle_procman_request(self, procman_request):
        self._log('got procman request: %s' % procman_request)

        if procman_request == "get_state":
            return self.get_state()

        if procman_request == "shutdown":
            return self.pm_shutdown()

        if procman_request == "run_process":
            return self.pm_run_process()

        # TODO dispatch other procman requests

        return "OK"

    def _handle_process_event(self, process_event):
        self._log('event from process received: %s' % process_event)
        #TODO handle received event
        #TODO update some "last recv'd heartbeat"

    def __rpc_to_process(self, cmd):
        request = str(cmd)
        self.req_socket.send(request)
        self._log("sent request to process: %s" % request)

        # TODO set and handle timeout
        message = self.req_socket.recv()
        self._log("Received reply from process: %s" % message)
        return message

    #
    # high-level operations
    #

    def pm_run_process(self):
        """
        Called to dispatch a 'run_process' request from the procman.
        This will launch the process.
        """

        self._log("pm_run_process requested")

        #
        # TODO pm_run_process. this will involve:
        # - checking whether the process is already running
        # - if not, launch the process according to the process definition
        # - if already running, do some form of sync so as to reflect its state
        #   appropriately here.

        # The launching below is preliminary; I'm using a flag to
        # facilitate ad hoc testing:
        do_launch = True

        if do_launch:
            # For the moment, only launching the process:
            pdef = self.pdef
            assert isinstance(pdef.cmd, list)
            args = pdef.cmd

            # add parameters to the args:
            args.extend(['--name', self.pdef.name])
            args.extend(['--heartbeat', str(self.pdef.heartbeat_interval)])
            # ports: note the REP/REQ and PUB/SUB relationships
            args.extend(['--rep_port', str(self.pxpr_req_port)])
            args.extend(['--pub_port', str(self.pxpr_sub_port)])

            self.os_proc = subprocess.Popen(args)

            msg = "process launched, pid = %s" % self.os_proc.pid
            self._log(msg)
        else:
            msg = "TODO: not launching process yet."

        return "OK: %s" % msg

    def pm_shutdown(self):
        """
        Called to dispatch a 'shutdown' request from the procman.
        This will make a request to corresponding remote process to shutdown
        via messaging. If the process does not respond by indicating it will
        in fact shutdown, then the process will be sent the KILL signal to
        force it terminate.
        """

        self._log("pm_shutdown requested")

        # ask process to shutdown:
        # TODO do what is indicated above about possible need to force
        # termination
        self.__rpc_to_process('shutdown')

        # TODO other clean-up

        self._keep_running = False
        return "OK: shutdown"

    def get_state(self):
        return self.__rpc_to_process('get_state')
