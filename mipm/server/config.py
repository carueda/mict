#!/usr/bin/env python

__author__ = 'Carlos Rueda'
__license__ = 'Apache 2.0'

from mipm.process import ProcessDef


class ManagerConfig(object):
    """
    Configuration for the ProcessManager
    """

    def __init__(self, pyobj):
        # endpoint
        assert 'endpoint' in pyobj
        self._endpoint = pyobj["endpoint"]

        # proc_defs
        assert 'processes' in pyobj
        self._processes = {}
        for p in pyobj["processes"]:
            pdef = ProcessDef(**p)
            if pdef.name in self._processes:
                raise Exception('%s: duplicate definition' % pdef.name)
            self._processes[pdef.name] = pdef

        # run: section with the processes to actually be run if the name
        # is associated with ON.
        # _run_processes keeps only the process to actually be run
        self._run_processes = []
        all_in_run = []   # to check for duplicates
        if 'run' in pyobj:
            for p in pyobj["run"]:
                assert isinstance(p, dict)
                assert len(p.keys()) == 1
                name = p.keys()[0]

                if not name in self._processes:
                    raise Exception('%s: undefined process' % name)

                if name in all_in_run:
                    raise Exception('%s duplicated in run section' % name)
                all_in_run.append(name)

                run = p.get(name, False)
                if not isinstance(run, bool):
                    assert isinstance(run, str)
                    assert run.lower() in ['on', 'off']
                    run = run is 'on'
                if run:
                    self._run_processes.append(name)

        # ports
        assert 'ports' in pyobj
        self._ports = pyobj['ports']
        assert isinstance(self._ports, dict)
        assert self._ports.has_key('pmpx_base')
        assert self._ports.has_key('pxpr_base')

    @property
    def endpoint(self):
        return self._endpoint

    @property
    def processes(self):
        """the dict of ProcessDef objects. The key is the name of the process.
        """
        return self._processes

    @property
    def run_processes(self):
        """the list of the names of the process that should be run
        """
        return self._run_processes

    @property
    def ports(self):
        return self._ports

    def __repr__(self):
        r = "%s(" % (self.__class__.__name__)
        r += "endpoint=%r, processes=%r, run=%r" % (
            self.endpoint,
            self.processes,
            self.run_processes)

        r += ", ports=%r" % (self.ports)

        r += ")"
        return r
