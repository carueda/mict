#!/usr/bin/env python

__author__ = 'Carlos Rueda'

"""
Process management on the client side, that is, for the actual managed
processes.
"""

from mipm.process import ProcessDef
from mipm.process import IProcess

from zope.interface import implements
import time

import argparse

from gevent_zeromq import zmq
from gevent import Greenlet


from mipm.util.mictlog import getLogger
log = getLogger(__name__)


def _create_argument_parser():
    """
    Creates the parser indicating required arguments to properly run a
    managed process.
    """

    parser = argparse.ArgumentParser(
            description="An MI Process")

    parser.add_argument("-n", "--name",
                        help="process name",
                        required=True)

    parser.add_argument("-hb", "--heartbeat",
                        help="heartbeat interval",
                        required=True,
                        type=float)

    parser.add_argument("-r", "--rep_port",
                        help="REP socket port",
                        required=True,
                        type=int)

    parser.add_argument("-p", "--pub_port",
                        help="PUB socket port",
                        required=True,
                        type=int)

    return parser


class Process(Greenlet):
    """
    Base class for client processes. Subclasses should in general only
    implement the pm_* operations as indicated in the IProcess interface.
    See example/proc/simple.py.
    """
    implements(IProcess)

    """
    This argparse.ArgumentParser instance should be used by processes to
    capture passed parameters from the process manager.
    """
    argument_parser = _create_argument_parser()

    def pm_shutdown(self):
        """
        Called to notify this process that it is being shutdown.
        """
        pass

    def pm_get_state(self):
        """
        In this class it returns a message indicating whether the process is
        running.
        """
        return 'Process %s running: %s' % (self.pdef.name, self._keep_running)

    def _log(self, m):
        """ad hoc provisional logger"""
        name = '|%s/%s' % ('PROCESS', self.pdef.name)
        print '%90s: %s' % (name, m)

    def __init__(self, opts):
        """
        Creates an instance of a managed process.

        @param opts The result of a call to parse_args on the attribute
        argument_parser of this class.

        """

        Greenlet.__init__(self)

        name = opts.name
        heartbeat = opts.heartbeat
        rep_port = opts.rep_port
        pub_port = opts.pub_port

        pdef = ProcessDef(name=name, heartbeat=heartbeat)

        self.pdef = pdef
        self._keep_running = True

        self._log("__init__")

        self.context = zmq.Context()

        #
        # sockets
        #

        # socket to reply to pproxy requests
        self.rep_socket = self.context.socket(zmq.REP)
        self._log("binding REP socket for pproxy requests...")
        self.rep_socket.bind("tcp://*:%d" % rep_port)

        # socket to publish heartbeats or other events
        self.pub_socket = self.context.socket(zmq.PUB)
        self.pub_socket.bind("tcp://*:%d" % pub_port)

        self.heartbeat_count = 0

    def _generate_heartbeat(self):
        self.heartbeat_count += 1
        msg = "%s: %d" % (self.pdef.name, self.heartbeat_count)
        self.pub_socket.send(msg)
        self._log('heartbeat sent: %s' % msg)

    def _run(self):
        self._log("_run")

        # use poller over the rep_socket, so we can try a reception with a
        # timeout, and thus avoid using recv(zmq.NOBLOCK), which would take
        # a lot of CPU.
        poller = zmq.Poller()
        poller.register(self.rep_socket, zmq.POLLIN)

        self. _log("process loop...")
        heartbeat_at = time.time() + self.pdef.heartbeat_interval

        while self._keep_running:
            pproxy_request = None

            socks = dict(poller.poll(
                    timeout=(1000 * self.pdef.heartbeat_interval)))

            if self.rep_socket in socks and socks[self.rep_socket] == \
                                            zmq.POLLIN:
                pproxy_request = self.rep_socket.recv()

            if time.time() > heartbeat_at:
                self._generate_heartbeat()
                heartbeat_at = time.time() + self.pdef.heartbeat_interval

            if pproxy_request is not None:
                response = self._handle_pproxy_request(pproxy_request)
                self.rep_socket.send(response)

    def _handle_pproxy_request(self, pproxy_request):
        self._log('request recv: %s' % pproxy_request)

        if pproxy_request == "get_state":
            return self.pm_get_state()

        if pproxy_request == "shutdown":
            return self._do_shutdown()

        # TODO dispatch other requests

        return "TODO: request '%s' not handled yet" % pproxy_request

    def _do_shutdown(self):
        self._keep_running = False
        self._log("_keep_running set to False")
        self.pm_shutdown()
        return "OK: shutdown"
