#!/usr/bin/env python

__author__ = 'Carlos Rueda'

"""
Entry point for the process manager program.
"""

import argparse
import yaml

from mipm.server.config import ManagerConfig
from mipm.server.procman import ProcessManager

from mipm.util.mictlog import getLogger
log = getLogger(__name__)

DEFAULT_CONFIG_FILE = "./config.yml"


def entry():
    parser = argparse.ArgumentParser(description="MI Process Manager")
    parser.add_argument("-c", "--configfile",
                        help="config file to use (default: %s)" %
                             DEFAULT_CONFIG_FILE,
                        default=DEFAULT_CONFIG_FILE)

    opts = parser.parse_args()

    configfile = opts.configfile

    log.info("loading %s" % configfile)
    stream = file(configfile, 'r')
    pyobj = yaml.load(stream)
    man_config = ManagerConfig(pyobj)

    log.debug(man_config)

    log.info('creating process manager...')
    procman = ProcessManager(man_config)

    log.info('starting process manager...')
    n = procman.start()
    log.info('process manager started with %s pproxys' % n)

    procman.join()


if __name__ == '__main__':
    entry()
