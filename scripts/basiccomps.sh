#
# Runs simple component interaction exercising basic zmq mechanisms.
#
                                                                     
bin/python mipm/basic/components/process.py &
bin/python mipm/basic/components/pproxy.py &
bin/python mipm/basic/components/procman.py &
bin/python mipm/basic/components/user.py 
