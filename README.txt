mict – MI Container
Carlos Rueda - MBARI

- The repo is currently at bitbucket.org:
    $ cd yourcodedir
    $ git clone https://carueda@bitbucket.org/carueda/mict.git
    $ cd mict

- If not already, create a virtualenv to isolate system site-packages:
	$ mkvirtualenv --no-site-packages --python=/usr/bin/python2.5 mict
	$ workon mict

- Run bootstrap.py to set up buildout:
	$ python bootstrap.py
    
- Run this whenever you change setup.py or buildout.cfg:
	$ bin/buildout

   Note: in case of any errors associated with required packages, try installing
    them explicitly. For example, I've seen a problem installing pyzmq under a
    python2.7 virtualenv in my OSX, apparently related with my zmq (under
    /usr/local) not being found (even though that setup.py has a directive to
    adddress that kind of problem). I solved it like so:
      $ C_INCLUDE_PATH=/usr/local/include easy_install-2.7 pyzmq
    Then I ran the rest of bin/buildout.

- Run tests:
	$ bin/nosetests

- Run simple component interaction exercising basic zmq mechanisms:
    $ scripts/basiccomps.sh

- Run a more complete process management exercise:
    In a terminal, run the process manager:
    $ bin/procman -c example/config/simple_config.yml

    In another terminal, run a simple user program that makes some requests
    to the process manager:
    $ bin/python mipm/basic/components/user.py

  More details in example/README.txt.
